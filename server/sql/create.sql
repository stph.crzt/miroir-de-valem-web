CREATE TABLE reflection (
  id VARCHAR PRIMARY KEY,
  passphrase VARCHAR NOT NULL, /** passphrase of reflection **/
  gallery VARCHAR CHECK gallery IN ('MDV', 'BY', 'BY-SA', 'BY-ND', 'BY-NC', 'BY-NC-SA', 'BY-NC-ND'), /** null -> private **/
  thumbnail BOOLEAN,
  reflections JSON, /* list of reflection sizes */
  owner VARCHAR NOT NULL
)
