#!/bin/bash

echo Transpiling...
tsc --project ts

echo Launching server...
node js/server.js --port 8081

echo Server running
