import sharp from 'sharp';
import { v4 as uuidv4 } from 'uuid';
import { Size } from './Size'
import { Reflection } from './Reflection'

export class Photo {
  static ORIENTATION_LANDSCAPE = 0
  static ORIENTATION_PORTRAIT = 1
  static DIRECTORY = 'uploads'
  static THUMBNAIL = new Size('Thumbnail', 480, 725)
  static SCREEN = new Size('Screen', 620, 874)
  static A6 = new Size('A6', 1240, 1748)
  static A2 = new Size('A2', 4960, 7016)
  static A0 = new Size('A0', 9933, 14043)
  static EXTERNAL_FRAME = 0.05 // 5% of image

  private photo: any
  private id: string
  private width: number
  private height: number
  private generated: Size[] // list of generated sizes
  private pixelized: Size[] // list of pixelized sizes

  constructor (request: any, result: any) {
    try {
      if (!request.files) {
        result.send({
          status: false,
          message: 'No file uploaded'
        })
      } else {
        this.id = uuidv4()
        this.photo = request.files.photo
        this.generated = []
        this.pixelized = []
        console.log(this.getName())
      }
    } catch (err) {
      result.status(500).send(err)
    }
  }

  public async init () {
    // Create screen file
    const file: sharp.Sharp = sharp(this.photo.data)
    const fileMetadata: sharp.Metadata = await file.metadata()
    this.width = fileMetadata.width
    this.height = fileMetadata.height
    await this.resize(Photo.SCREEN, false)
  }

  public getId(): string {
    return this.id
  }

  /**
  resize a photo in order to prepare pixelize command
  - resize
  - add brightness
  - save to file
  **/
  public async resize(size: Size, brightness: boolean = true) {
    let file: sharp.Sharp = sharp(this.photo.data)

    // Resizing
    const width: number = this.getWidth(size)
    const height: number = this.getHeight(size)
    file = file.resize(width, height, {"fit": "fill"})

    // Add brightness
    if (brightness) {
      file = file.modulate({brightness: 1.5}).normalize()
    }
    await file.toFile(this.getPath(size).valueOf())
    this.generated.push(size)
    console.log(`${this.getPath(size)} generated [width:${width} height:${height} ratio:${this.getRatio()}]`)
  }

  public getName(): string {
    return this.photo.name
  }

  public getMimetype(): string {
    return this.photo.mimetype
  }

  public getSize(): number {
    return this.photo.size
  }

  public getPath(size: Size): string {
    return `./${Photo.DIRECTORY}/${this.getId()}_${size.getName()}_${this.getName()}`
  }

  public getUrl(size: Size): string {
    return `/${Photo.DIRECTORY}/${this.getId()}_${size.getName()}_${this.getName()}`
  }

  private getRatio(): number {
      return this.width / this.height
  }

  public getOrientation(): number {
    if (this.width > this.height) {
      return Photo.ORIENTATION_LANDSCAPE
    }
    else {
      return Photo.ORIENTATION_PORTRAIT
    }
  }

  private getDim(size: Size): any {
    let width: number
    let height: number
    if  (this.getOrientation() === Photo.ORIENTATION_PORTRAIT) {
      const sizeRatio = size.getWidth() / (size.getHeight() - Reflection.SIGNATURE_HEIGHT)
      if (this.getRatio() >= sizeRatio) {
        width = size.getWidth(Photo.EXTERNAL_FRAME)
        height = Math.floor(width / this.getRatio())
      }
      else {
        height = size.getHeight(Photo.EXTERNAL_FRAME) - Reflection.SIGNATURE_HEIGHT
        width = Math.floor(height * this.getRatio())
      }
    }
    else {
      const sizeRatio = size.getHeight() / (size.getWidth() - Reflection.SIGNATURE_HEIGHT)
      if (this.getRatio() >= sizeRatio) {
        width = size.getHeight(Photo.EXTERNAL_FRAME)
        height = Math.floor(width / this.getRatio())
      }
      else {
        height = size.getWidth(Photo.EXTERNAL_FRAME) - Reflection.SIGNATURE_HEIGHT
        width = Math.floor(height * this.getRatio())
      }
    }
    return {width, height}
  }
  public getWidth(size: Size = null): number {
    if (! size) {
      return this.width
    }
    else {
      return this.getDim(size).width
    }
  }
  public getHeight(size: Size = null): number {
    if (! size) {
      return this.width
    }
    else {
      return this.getDim(size).height
    }
  }

  public isGenerated(size: Size): boolean {
    return this.generated.includes(size)
  }

  public addPixelized(size: Size) {
    this.pixelized.push(size)
  }

  public isPixelized(size: Size): boolean {
    return this.pixelized.includes(size)
  }
}
