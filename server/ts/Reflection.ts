import sharp from 'sharp';
import { Photo } from './Photo'
import { Size } from './Size'

import conf from './conf.json'

export class Reflection {

  static PIXELIZE = conf.pixelize.path
  static SIGNATURE = 'res/signature.jpeg'
  static SIGNATURE_HEIGHT = 182
  static TMP = '/tmp'

  private photo: Photo

  constructor(photo: Photo) {
    this.photo = photo
  }

  public async init() {
    //photo.get
  }

  public async pixelize(size: Size) {
    //if (this.photo.isGenerated(size)) {
      const suffix = 'pixelized.jpeg'
      const inputFile = this.photo.getPath(size)
      const tmpFile = `${Reflection.TMP}/${this.photo.getId()}.${suffix}`
      const outputFile = `${inputFile}.${suffix}`
      const outputUrl = `${this.photo.getUrl(size)}.${suffix}`

      if (! this.photo.isGenerated(size)) {
        await this.photo.resize(size)
      }

      if (! this.photo.isPixelized(size)) {

        // Creating pixelized image
        const util = require('util')
        const exec = util.promisify(require('child_process').exec)
        const cmd = `DISPLAY=:0 ${Reflection.PIXELIZE}/pixelize -i ${inputFile} -o ${tmpFile} -w ${size.getThumbnailWidth()} -h ${size.getThumbnailHeight()} -d ${Reflection.PIXELIZE}/pic_db.dat`
        const { stdout, stderr } = await exec(cmd)
        console.error('stderr:', stderr)
        console.log('stdout:', stdout)

        // Adding signature
        let width: number
        if (this.photo.getOrientation() === Photo.ORIENTATION_PORTRAIT) {
          width = size.getWidth()
        }
        else {
          width = size.getHeight()
        }
        let height: number = this.photo.getHeight(size) + size.getThumbnailHeight() + Reflection.SIGNATURE_HEIGHT
        console.log (width)
        console.log (height)
        const photoWithSignature: Buffer = await sharp({
          create: {
            width: width,
            height: height,
            channels: 3,
            background: { r: 0, g: 0, b: 0 }
          }
        })
        .composite([
          { input: tmpFile, gravity: 'north' },
          { input: Reflection.SIGNATURE, gravity: 'south' }
        ])
        .jpeg()
        .toBuffer()

        // Adding black frame
        if (this.photo.getOrientation() === Photo.ORIENTATION_PORTRAIT) {
          height = size.getHeight()
        }
        else {
          height = size.getWidth()
        }
        console.log (width)
        console.log (height)
        const photoWithFrame: sharp.Sharp = sharp({
          create: {
            width: width,
            height: height,
            channels: 3,
            background: { r: 0, g: 0, b: 0 }
          }
        })
        .composite([
          { input: photoWithSignature }
        ])
        .jpeg()

        await photoWithFrame.toFile(outputFile)

        this.photo.addPixelized(size)
      }

    return outputUrl
  }

}
