import { promises as fs } from 'fs';
import { Photo } from './Photo'
const MAX_AGE: number = 6

async function purge () {
  const dir: string = Photo.DIRECTORY
  const fileList = await fs.readdir(dir)
  for (const fileName of fileList) {
    const filePath = `${dir}/${fileName}`
    const fileStats = await fs.stat(filePath)
    const ageOfFile: number = (new Date().getTime() - fileStats.atime.getTime()) / 3600000
    if (ageOfFile > MAX_AGE) {
      fs.unlink(filePath)
      console.log(`${filePath} purged`)
    }
    else {
      console.log(`${filePath} not purged (age : ${Math.round(ageOfFile)} hours)`)
    }
  }
}

purge();
