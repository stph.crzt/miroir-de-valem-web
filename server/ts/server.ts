#!/usr/bin/env node

import express from 'express'
import fileUpload from 'express-fileupload'
import { Photo } from './Photo'
import { Reflection } from './Reflection'
import { Size } from './Size'

const app = express()
app.use(express.urlencoded({ extended: true }))
app.use(express.json())
let port: Number = 8081


/** Arguments
  --port
**/
process.argv.forEach((val, index) => {
  if (val === '--port' || val === '-p') {
    let p: Number = parseInt(process.argv[index + 1])
    if (p) {
      port = p
      console.log(`Port : ${port}`)
    }
    else {
      console.log(`Error of port specification (--port) ; reset to default port ${port}`)
    }
  }
})

/** Checking and loading config file
conf/conf.json
**/

// enable files upload
app.use(fileUpload({ createParentPath: true }))
app.use('/uploads', express.static('uploads'))
app.use('/res', express.static('res'))

app.get('/', async (request, result) => {
  result.render('index.ejs')
})

app.get('/photo', async (request, result) => {
  result.render('photo.ejs')
})

let photo: Photo

app.post('/upload', async (request, result) => {
  photo = new Photo(request, result)
  await photo.init()
  result.render('uploaded.ejs', {

    name: photo.getName(),
    path: photo.getUrl(Photo.SCREEN),
    mimetype: photo.getMimetype(),
    size: photo.getSize()
  })
})

let reflection: Reflection

app.get('/reflection/:size', async (request, result) => {
  reflection = new Reflection(photo)
  // List of existing size in the application
  const sizesList: Size[] = [Photo.A6, Photo.A2, Photo.A0]
  // Find the required size (return first element of the size tab if not found)
  const sizeNamesList: string[] = sizesList.map (s => s.getName())
  let currentSizeIndex: number = Math.max(sizeNamesList.indexOf(request.params.size), 0)
  const currentSize: Size = sizesList[currentSizeIndex]
  // Build tab with other existing sizes in order to build menu
  const current = sizesList.splice (currentSizeIndex, 1)[0]
  const others = sizesList
  // Pixelize and rendering
  const path = await reflection.pixelize(currentSize)
  console.log(path)
  result.render('reflection.ejs', {
    path: path,
    size: current,
    others: others,
    passphrase: false
  })
})

app.post('/reflection/:size/save', async (request, result) => {

  console.log(request.body)
  const passphraseInput: string = request.body.passphraseInput
  console.log(passphraseInput)

  result.json(request.body)

})

app.listen(port)
