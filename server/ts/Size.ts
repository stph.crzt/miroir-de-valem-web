export class Size {
  static THUMBNAIL_REFERENCE = 140 // Number of thumbnail for a portrait image width TODO -> conf/conf.json
  static THUMBNAIL_RATIO = 720 / 480 // height and width of Valem thumbnails TODO -> conf/conf.json
  private name: string
  private width: number
  private height: number

  constructor (name: string, width: number, height: number) {
    this.name = name
    // Size should always be declared as portrait
    if (width > height) {
      console.log('Warning : width should be superior to height, auto-correction') // TODO manage warning and errors
      this.width = height
      this.height = width
    }
    else {
      this.width = width
      this.height = height
    }
  }

  /**
  return the width or height of size
  argument : an optional reduction between 0 and 0.5
  note : reduction can't be superior to 0.5
  **/
  public getWidth(reduction: number = 0): number {
      if (reduction < 0) reduction = 0
      if (reduction > 0.5) reduction = 0.5
      return Math.floor(this.width - this.width * reduction)
  }
  public getHeight(reduction: number = 0): number {
    if (reduction < 0) reduction = 0
    if (reduction > 0.5) reduction = 0.5
    return Math.floor(this.height - this.height * reduction)
  }

  public getName() {
    return this.name
  }

  public getThumbnailWidth(): number {
    return Math.round(this.width / Size.THUMBNAIL_REFERENCE)
  }

  public getThumbnailHeight(): number {
    return Math.round(this.getThumbnailWidth() * Size.THUMBNAIL_RATIO)
  }

}
