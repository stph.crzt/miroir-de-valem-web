#!/bin/bash

echo Transpiling...
tsc --project ts

echo Purge testing
touch -d "72 hours ago" ./uploads/fileToPurge
touch ./uploads/fileToKeep
./purge.sh
