# INSTALL

## Node

v12.18.2
https://github.com/nodesource/distributions/blob/master/README.md#debinstall

curl -sL https://deb.nodesource.com/setup_lts.x | sudo -E bash -
sudo apt-get install -y nodejs

## Modules

npm install express ejs express-fileupload uuid sharp


## TS

sudo npm install -g typescript
sudo npm install -g @types/node @types/express @types/sharp @types/express-fileupload @types/uuid

## conf.json (edit to server path)
cp conf/conf.json ts/conf.json

## Uploads
mkdir uploads (ou add .gitinclude)

## Cron

crontab -e
1 * * * * /path-to/miroir-de-valem-web/server/purge.sh
