# Acteurs
Utilisateur
Gestionnaire (des versions imprimées)
Administrateur

# Objets

Photo : photo originale 
Reflet : dessin pixelisées composé des photos 

# Fonctions

Solution proposée, sans authentification.

F1.1. L'utilisateur charge une photo 
- via un bouton charger
- depuis l'accueil
- depuis son disque dur
- il y a une information sur le droit d'auteur et le droit à l'image
- il y a des indications de formats de fichier (jpg, png), de résolution et/ou de nombres de pixels, de taille maximale
- le système vérifie que le fichier chargé correspond bien aux pré-requis, il exprime des avertissements non bloquants et des erreurs bloquantes
- le système stocke la photo dans l'espace temporaire des photos avec un timestamp de création, un id, une miniature (exemple : 300px de hauteur ou de largeur max), un timestamp d'envoi
- le système affiche une page de validation avec la miniature

F1.2. L'utilisateur modifie une photo
- via un bouton modifier 
- une fois qu'une photo a été chargée
- suppression de la photo de l'espace de stockage temporaire
- retour à l'accueil

F1.3. L'utilisateur génère un reflet
- via un bouton générer
- une fois qu'une photo a été chargée
- le système annonce un délai d'attente (par exemple : 3 minutes) 
- le système génère le reflet et le stocke dans l'espace temporaire des reflets avec un timestamp, un id (identique à celui de la photo correspondante), une miniature, une formule magique (valeur par défaut générée automatiquement)  
- le système affiche le reflet et propose de le télécharger
- le système propose de télécharger la version haute résolution pour impression ?
- le système propose à l'utilisateur de conserver son reflet pour le faire imprimer ou pour le partager, il doit pour cela entrer une formule magique (passphrase) (un générateur de formule magique peut faire une proposition)
- le système propose à l'utilisateur de partager son reflet
- le système propose de faire un don via Helloasso

F2.1. L'utilisateur fait un don et éventuellement obtient un reflet au format papier en contre-partie
- via un bouton soutenir le projet et obtenir votre reflet au format papier
- une fois qu'un reflet a été généré
- le système renvoie vers Helloasso
- le système peut proposer en contre-partie une impression papier : l'utilisateur entre la formule magique dans ce cas

F2.2. Le gestionnaire gère l'envoi des reflets 
- il dispose de la liste des reflets
- il peut rechercher par formule magique et par date

F2.3. Le gestionnaire confirme l'envoi d'un reflet au format papier
- il récupère les informations depuis Helloasso (non géré par le système)
- il peut marquer le reflet comme ayant été envoyé au format papier (avec un timestamp)

F3.1 L'utilisateur partage un de ses reflets
- qui a été conservé
- via un bouton partager
- l'utilisateur choisir une formule magique
- l'utilisateur choisit une licence CC ou Art Libre (ou Miroir De Valem, licence de non diffusion hors MDV)
- l'utilisateur choisit une formule magique (phrase secrète) de suppression (on lui conseille de la conserver)
- le reflet est déplacé de l'espace temporaire vers l'espace permanent, la formule magique est ajoutée

F3.2 L'utilisateur consulte la galerie des reflets partagés

F3.3 L'utilisateur supprime un de ces reflets
- en saisissant sa formule magique

F4.1 Le système supprime les photos des espaces temporaires 
- Si les photos ont plus de 24h 

F4.2 Le système supprime les reflets des espaces temporaires 
- Si les reflets ont plus de 24h 

F4.2 Le système sauvegarde les reflets des espaces temporaires et permanents
- Toutes les 24h


